part of './pages.dart';

abstract class Routes {
  static const HOME = '/home';
  static const NEWGAME = '/newGame';
  static const PLAYGAME = '/playGame';
  static const JOINGAME = '/joinGame';
  static const QRSCANNER = '/qrScanner';
}
