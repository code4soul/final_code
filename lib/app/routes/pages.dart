import 'package:final_code/app/modules/newGame/page.dart';
import 'package:final_code/app/modules/newGame/binding.dart';
import 'package:final_code/app/modules/playGame/binding.dart';
import 'package:final_code/app/modules/playGame/page.dart';
import 'package:final_code/app/modules/joinGame/page.dart';
import 'package:final_code/app/modules/joinGame/binging.dart';
import 'package:final_code/app/modules/qrScanner/page.dart';
import 'package:final_code/app/modules/home/page.dart';
import 'package:get/get.dart';
import '../app.dart';

part './routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.HOME, page: () => HomePage()),
    GetPage(name: Routes.NEWGAME, page: () => NewGamePage(), binding: NewGameBinding()),
    GetPage(name: Routes.PLAYGAME, page: () => PlayGamePage(), binding: PlayGameBinding()),
    GetPage(name: Routes.JOINGAME, page: () => JoinGamePage(), binding: JoinGameBinding()),
    GetPage(name: Routes.QRSCANNER, page: () => QRScanner()),
  ];
}
