import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';

import '../../../../firebase_options.dart';

class FireStoreProvider extends GetConnect {
  late FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  void onInit() async {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);
    super.onInit();
  }

  createNewGame(String hostName) async {
    DocumentReference result = await db.collection('games').add({
      "rangeMin": 1,
      "rangeMax": 100,
      "isStarted": false,
      "players": [hostName],
      "createdAt": DateTime.now(),
    });
    return result;
  }

  // 加入遊戲
  joinNewGame(String gameId, List<String> players) async {
    await db.collection('games').doc(gameId).update({"players": players});
  }

  // 開始遊戲
  startPlayGame(String gameId, List<String> newPlayersArray) async {
    Random ranom = new Random();
    int answer = ranom.nextInt(100) + 1;
    await db.collection('games').doc(gameId).update({
      "isStarted": true,
      "answer": answer,
      "players": newPlayersArray,
      "callPlayer": newPlayersArray[0],
    });
  }

  // listen game node
  Stream<DocumentSnapshot<Object?>> getGameInfo(String gameId) {
    return db.collection('games').doc(gameId).snapshots();
  }

  // set call number, update game info
  setCallNumber(String gameId, Map<String, dynamic> data) async {
    await db.collection('games').doc(gameId).update(data);
  }
}
