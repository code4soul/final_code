import 'package:json_annotation/json_annotation.dart';

part 'game_model.g.dart';

@JsonSerializable()
class GameModel {
  int answer;
  int rangeMax;
  int rangeMin;
  bool isStarted;
  bool isEnded;
  List<String> history;
  List<String> players;
  String callPlayer;

  GameModel({
    this.answer = 0,
    required this.rangeMax,
    required this.rangeMin,
    required this.isStarted,
    this.isEnded = false,
    this.history = const [],
    this.players = const [],
    this.callPlayer = "",
  });

  factory GameModel.fromJson(Map<String, dynamic> json) => _$GameModelFromJson(json);
  Map<String, dynamic> toJson() => _$GameModelToJson(this);
}
