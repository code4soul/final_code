// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameModel _$GameModelFromJson(Map<String, dynamic> json) => GameModel(
      answer: json['answer'] as int? ?? 0,
      rangeMax: json['rangeMax'] as int,
      rangeMin: json['rangeMin'] as int,
      isStarted: json['isStarted'] as bool,
      isEnded: json['isEnded'] as bool? ?? false,
      history: (json['history'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      players: (json['players'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      callPlayer: json['callPlayer'] as String? ?? "",
    );

Map<String, dynamic> _$GameModelToJson(GameModel instance) => <String, dynamic>{
      'answer': instance.answer,
      'rangeMax': instance.rangeMax,
      'rangeMin': instance.rangeMin,
      'isStarted': instance.isStarted,
      'isEnded': instance.isEnded,
      'history': instance.history,
      'players': instance.players,
      'callPlayer': instance.callPlayer,
    };
