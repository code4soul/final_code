import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_code/app/data/provider/firebase/firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import '../../data/model/game_model.dart';
import 'package:text_to_speech/text_to_speech.dart';
import 'package:vibration/vibration.dart';

class PlayGameController extends GetxController with StateMixin<Rx<GameModel>>{
  final fireStoreProvider = Get.find<FireStoreProvider>();
  Rx<GameModel>? game;
  RxBool isLoading = true.obs;
  TextToSpeech tts = TextToSpeech();
  String gameIdx = "";
  String name = "";
  late StreamSubscription<DocumentSnapshot<Object?>> gameInfoListener;
  var logger = Logger();
  Rx<int> deno = 98.obs;

  Future getRoomInfo(String gameId) async {
    gameIdx = gameId;
    final Stream<DocumentSnapshot> result = fireStoreProvider.getGameInfo(gameId);
    gameInfoListener = result.listen((event) {
      if(game == null) {
        game = GameModel.fromJson(event.data() as Map<String, dynamic>).obs;
        tts.speak("遊戲開始");
      } else {
        game?.value = GameModel.fromJson(event.data() as Map<String, dynamic>);
      }

      Vibration.vibrate();
      if(game?.value.isEnded == true) {
        gameInfoListener.cancel();
        tts.speak("遊戲結束，掰掰");
      } else {
        deno.value = game!.value.rangeMax - game!.value.rangeMin - 1;
        tts.speak("${game?.value.rangeMin} 到 ${game?.value.rangeMax}");
      }
      isLoading.value = false;
    });
  }

  void setCallNumber(String number) async {
    if(game != null) {
      final inputValue = int.parse(number);
      int answer = game?.value.answer ?? 0;
      Map<String, dynamic> data = {};

      List<String> history = game?.value.history ?? [];
      List<String> newHistory = ["${name}: ${number}", ...history];
      String callPlayer = game?.value.players[0] ?? "";
      int index = game?.value.players.indexOf(name) ?? 0;
      if((index+1) < game!.value.players.length.toInt()) {
        callPlayer = game?.value.players[(index+1)] ?? "";
      }

      if(inputValue == answer) {
        data = {"isEnded": true, "history": newHistory};
      } else if(inputValue > answer) {
        data = {"rangeMax": inputValue, "history": newHistory, "callPlayer": callPlayer};
      } else {
        data = {"rangeMin": inputValue, "history": newHistory, "callPlayer": callPlayer};
      }
      await fireStoreProvider.setCallNumber(gameIdx, data);
    }
  }

  @override
  void onInit() {
    super.onInit();
    var gameId = Get.parameters['game_id'] ?? "";
    name = Get.parameters['name'] ?? '';
    getRoomInfo(gameId);
    tts.setLanguage('zh-TW');
  }

  @override
  void onClose() {
    logger.w("onClose");
    gameInfoListener.cancel();
    super.onClose();
  }
}
