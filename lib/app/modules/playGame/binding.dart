import 'package:final_code/app/modules/playGame/controller.dart';
import 'package:get/get.dart';

class PlayGameBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PlayGameController());
  }
}
