import 'dart:math';

import 'package:final_code/app/modules/playGame/controller.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import '../../routes/pages.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/percent_indicator.dart';

class PlayGamePage extends GetView<PlayGameController> {
  PlayGamePage({Key? key}) : super(key: key);
  final numInputController = TextEditingController();

  callNumber() {
    if (numInputController.text.isEmpty) {
      Get.snackbar("Error", "請輸入數字");
    } else {
      int min = controller.game?.value.rangeMin ?? 0;
      int max = controller.game?.value.rangeMax ?? 100;
      if (int.parse(numInputController.text) <= min) {
        Get.snackbar("Error", "輸入數字太小");
        return;
      }
      if (int.parse(numInputController.text) >= max) {
        Get.snackbar("Error", "輸入數字太大");
        return;
      }
      controller.setCallNumber(numInputController.text);
      numInputController.text = "";
    }
  }

  showEndedGame() {
    return Center(
      child: Column(
        children: [
          Text("End Game",
              style:
                  GoogleFonts.rockSalt(fontSize: 24, color: Colors.blueGrey)),
          Text(
            "Final Answer",
            style: GoogleFonts.rockSalt(fontSize: 40, color: Colors.teal),
          ),
          Text(
            "${controller.game?.value.answer}",
            style: GoogleFonts.rockSalt(fontSize: 60, color: Colors.deepOrange),
          ),
          ElevatedButton(
              onPressed: () {
                Get.offNamedUntil(Routes.HOME, (route) => false);
              },
              child: const Text("再來一局!"))
        ],
      ),
    );
  }

  List<Widget> generateList() {
    List<Widget> historyList = [];
    if (controller.game?.value.history != null) {
      List<String> list = controller.game?.value.history ?? [];
      for (String log in list) {
        historyList.add(Text(log,
            style: GoogleFonts.rockSalt(fontSize: 13, color: Colors.blueGrey)));
      }
    }
    return historyList;
  }

  List<Widget> generateInputArear() {
    List<Widget> widgets = [];
    if (controller.game?.value.callPlayer == controller.name) {
      widgets.add(TextField(
        controller: numInputController,
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        style: GoogleFonts.rockSalt(fontSize: 55, color: Colors.teal),
      ));
      widgets.add(ElevatedButton(
          onPressed: () {
            callNumber();
          },
          child: Text("送出")));
    } else {
      widgets.add(Text("等待 ${controller.game?.value.callPlayer} 輸入密碼"));
    }
    return widgets;
  }

  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(title: const Text("Play Game")),
          body: Stack(children: [
            Positioned(
              right: -50,
              bottom: -100,
              child: Text("${random.nextInt(100)}",
                  style: GoogleFonts.rockSalt(
                      fontSize: 150, color: Colors.black12)),
            ),
            Positioned(
              right: 50,
              bottom: 50,
              child: Text("${random.nextInt(100)}",
                  style: GoogleFonts.rockSalt(
                      fontSize: 180, color: Colors.black12)),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: controller.isLoading.value
                  ? Container()
                  : controller.game?.value.isEnded == true
                      ? showEndedGame()
                      : Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularPercentIndicator(
                                    radius: 40.0,
                                    lineWidth: 30.0,
                                    animation: true,
                                    percent: 1 / controller.deno.value,
                                    center: Text(
                                      "${((1 / controller.deno.value) * 100).toPrecision(2)}%",
                                      style: GoogleFonts.lato(
                                          fontSize: 12.0,
                                          color: Colors.redAccent),
                                    )),
                                SizedBox(width: 20),
                                Text(
                                    "${controller.game?.value.rangeMin} - ${controller.game?.value.rangeMax}",
                                    style: GoogleFonts.architectsDaughter(
                                        fontSize: 35))
                              ],
                            ),
                            ...generateInputArear(),
                            SingleChildScrollView(
                              child: Column(
                                children: generateList(),
                              ),
                            ),
                          ],
                        ),
            ),
          ]),
        ));
  }
}
