import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_code/app/data/provider/firebase/firestore.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../data/model/game_model.dart';

class NewGameController extends GetxController {
  final fireStoreProvider = Get.find<FireStoreProvider>();
  RxBool isGameCreated = false.obs;
  RxString? gameId;
  Logger logger = Logger();
  Rx<GameModel>? game;
  late StreamSubscription<DocumentSnapshot<Object?>> gameInfoListener;

  @override
  void onInit() {
    super.onInit();
  }

  createNewGame(String hostName) async {
    DocumentReference<Map<String, dynamic>> result =
        await fireStoreProvider.createNewGame(hostName);
    if (result != null) {
      if (gameId?.value == null) {
        gameId = "".obs;
      }
      gameId?.value = result.id;
      isGameCreated.value = true;

      final query = fireStoreProvider.getGameInfo(result.id);
      gameInfoListener = query.listen((event) {
        if (event.exists) {
          print(event.data());
          if (game == null) {
            game = GameModel.fromJson(event.data() as Map<String, dynamic>).obs;
          } else {
            game?.value =
                GameModel.fromJson(event.data() as Map<String, dynamic>);
          }
        } else {
          gameInfoListener.cancel();
        }
      });
    }
  }

  Future<void> startPlayGame(String roomId) async {
    List<String> newPlayersArray = game?.value.players ?? [];
    newPlayersArray.shuffle();
    var result = await fireStoreProvider.startPlayGame(roomId, newPlayersArray);
  }

  @override
  void onClose() {
    logger.i("onClose");
    super.onClose();
  }
}
