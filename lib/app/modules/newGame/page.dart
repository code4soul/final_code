import 'dart:math';

import 'package:final_code/app/modules/newGame/controller.dart';
import 'package:flutter/material.dart';
import 'package:final_code/app/routes/pages.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class NewGamePage extends GetView<NewGameController> {
  NewGamePage({Key? key}) : super(key: key);
  final nameController = TextEditingController();

  generateUsers() {
    List<Widget> lists = [];
    for (String item in controller.game?.value.players ?? []) {
      lists.add(
        Text(
          item,
          style: GoogleFonts.lato(
              color: nameController.text == item
                  ? Colors.teal
                  : Colors.deepOrange),
        ),
      );
    }
    return lists;
  }

  List<Widget>? onGameCreated() {
    if (controller.isGameCreated.value) {
      return [
        QrImage(data: controller.gameId?.value ?? ""),
        Text(controller.gameId?.value ?? "",
            style: GoogleFonts.lato(color: Colors.teal)),
        SizedBox(height: 20),
        const Text("參與者", style: TextStyle(fontSize: 22, color: Colors.cyan)),
        ...generateUsers(),
        ElevatedButton(
            onPressed: () {
              controller.startPlayGame(controller.gameId?.value ?? "");
              Get.offNamed(Routes.PLAYGAME, parameters: {
                "game_id": controller.gameId?.value ?? "",
                "name": nameController.text
              });
            },
            child: const Text("開始！！")),
      ];
    }
    return null;
  }

  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: AppBar(
          title: const Text("Start a New Game"),
        ),
        body: Stack(children: [
          Positioned(
            right: -50,
            bottom: -100,
            child: Text("${random.nextInt(100)}",
                style:
                    GoogleFonts.rockSalt(fontSize: 150, color: Colors.black12)),
          ),
          Positioned(
            right: 50,
            bottom: 50,
            child: Text("${random.nextInt(100)}",
                style:
                    GoogleFonts.rockSalt(fontSize: 180, color: Colors.black12)),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  style: GoogleFonts.lato(),
                  decoration: const InputDecoration(labelText: "玩家名"),
                  controller: nameController,
                  readOnly: controller.isGameCreated.value,
                  textAlign: TextAlign.center,
                ),
                controller.isGameCreated.value != true
                    ? ElevatedButton(
                        onPressed: () {
                          controller.createNewGame(nameController.text);
                        },
                        child: const Text("建立"))
                    : Container(),
                ...?onGameCreated(),
              ],
            ),
          ),
        ])));
  }
}
