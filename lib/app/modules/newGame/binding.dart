import 'package:final_code/app/modules/newGame/controller.dart';
import 'package:get/get.dart';

class NewGameBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(NewGameController());
  }
}
