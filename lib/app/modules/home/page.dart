import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../routes/pages.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('終極密碼'),
      ),
      body: Stack(
        children: [
          Positioned(right: -50, bottom: -100, child: Text("${random.nextInt(100)}", style: GoogleFonts.rockSalt(fontSize: 150, color: Colors.black12)),),
          Positioned(right: 50, bottom: 50, child: Text("${random.nextInt(100)}", style: GoogleFonts.rockSalt(fontSize: 180, color: Colors.black12)),),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                const SizedBox(height: 100),
                SizedBox(
                    width: 200,
                    height: 100,
                    child: ElevatedButton(
                      onPressed: () {
                        Get.toNamed(Routes.NEWGAME);
                      },
                      child: const Text("建立新遊戲"),
                    )),
                const Divider(),
                SizedBox(
                    width: 200,
                    height: 100,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey
                        ),
                        onPressed: () {
                          Get.toNamed(Routes.JOINGAME);
                        },
                        child: const Text("加入連線遊戲")))
              ],
            ),
          )
        ],
      ),
    );
  }
}
