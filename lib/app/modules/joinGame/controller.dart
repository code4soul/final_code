import 'dart:async';
import 'dart:ffi';

import 'package:final_code/app/data/provider/firebase/firestore.dart';
import 'package:final_code/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../../data/model/game_model.dart';
import '../../routes/pages.dart';
import 'package:vibration/vibration.dart';

class JoinGameController extends GetxController {
  final fireStoreProvider = Get.find<FireStoreProvider>();
  RxBool isGameJoined = false.obs;
  RxBool isGameIdError = false.obs;
  String? gameIdx;
  Rx<GameModel>? game;
  late StreamSubscription<DocumentSnapshot<Object?>> gameInfoListener;

  @override
  void onInit() {
    super.onInit();
  }

  Future joinGame(String gameId, String name) async {
    gameIdx = gameId;
    final result = fireStoreProvider.getGameInfo(gameId);

    gameInfoListener = result.listen((event) {
      if (event.exists) {
        if (game == null) {
          game = GameModel.fromJson(event.data() as Map<String, dynamic>).obs;
        } else {
          game?.value =
              GameModel.fromJson(event.data() as Map<String, dynamic>);
        }
        if (isGameJoined.value == false) {
          final players = [name, ...?game?.value.players];
          fireStoreProvider.joinNewGame(gameId, players);
        }
        if (game?.value.isStarted == true) {
          Vibration.vibrate();
          gameInfoListener.cancel();
          Get.offNamed(Routes.PLAYGAME, parameters: {"game_id": gameId, "name": name});
        }
        isGameJoined.value = true;
        isGameIdError.value = false;
      } else {
        isGameIdError.value = true;
        gameInfoListener.cancel();
      }
    });
  }

  @override
  void dispose() {
    gameInfoListener.cancel();
    super.dispose();
  }
}
