import 'package:final_code/app/modules/joinGame/controller.dart';
import 'package:get/get.dart';

class JoinGameBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(JoinGameController());
  }
}
