import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import './controller.dart';
import 'package:final_code/app/routes/pages.dart';

class JoinGamePage extends GetView<JoinGameController> {
  JoinGamePage({Key? key}) : super(key: key);
  final nameController = TextEditingController();
  final gameIdController = TextEditingController();

  generateUsers() {
    List<Widget> lists = [];
    for (String item in controller.game?.value.players ?? []) {
      lists.add(
        Text(
          item,
          style: GoogleFonts.lato(
              color: nameController.text == item
                  ? Colors.teal
                  : Colors.deepOrange),
        ),
      );
    }
    return lists;
  }

  List<Widget>? onGameCreated() {
    if (controller.isGameJoined.value) {
      return [
        const Text("參與者", style: TextStyle(fontSize: 22, color: Colors.cyan)),
        ...generateUsers(),
      ];
    }
    return null;
  }

  openScanner() async {
    final code = await Get.toNamed(Routes.QRSCANNER);
    gameIdController.text = code;
  }

  joinGame() {
    controller.joinGame(gameIdController.text, nameController.text);
  }

  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: AppBar(
          title: const Text("Join a New Game"),
        ),
        body: Stack(children: [
          Positioned(
            right: -50,
            bottom: -100,
            child: Text("${random.nextInt(100)}",
                style:
                    GoogleFonts.rockSalt(fontSize: 150, color: Colors.black12)),
          ),
          Positioned(
            right: 50,
            bottom: 50,
            child: Text("${random.nextInt(100)}",
                style:
                    GoogleFonts.rockSalt(fontSize: 180, color: Colors.black12)),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  decoration: const InputDecoration(labelText: "玩家名"),
                  controller: nameController,
                  readOnly: controller.isGameJoined.value,
                  textAlign: TextAlign.center,
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: "Game #id",
                      suffix: IconButton(
                          onPressed: () {
                            openScanner();
                          },
                          icon: const Icon(Icons.qr_code)),
                      errorText: controller.isGameIdError == true
                          ? "Game Id Error"
                          : null),
                  controller: gameIdController,
                  readOnly: controller.isGameJoined.value,
                  textAlign: TextAlign.center,
                  style: controller.isGameIdError == true
                      ? const TextStyle(color: Colors.red)
                      : null,
                ),
                ElevatedButton(
                    onPressed: controller.isGameJoined.value
                        ? null
                        : () {
                            joinGame();
                          },
                    child: const Text("加入")),
                ...?onGameCreated(),
              ],
            ),
          ),
        ])));
  }
}
